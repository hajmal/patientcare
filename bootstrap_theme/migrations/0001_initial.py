# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Carousel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('carousel_group', models.SlugField(help_text=b'Use the same group name for carousels that display together.')),
                ('caption', models.CharField(help_text=b'The large caption text.', max_length=40)),
                ('lead', models.TextField(help_text=b'The marketing text just below the caption.')),
                ('image', models.ImageField(help_text=b'The big banner image to display.', upload_to=b'carousel')),
                ('ordering', models.PositiveSmallIntegerField(help_text=b'The carousel items are sorted by this number.')),
                ('button_text', models.CharField(default=b'Learn more', help_text=b'The text to place on the button.', max_length=30)),
                ('button_url', models.CharField(default=b'list-machines', help_text=b'The destination of the button.', max_length=20)),
                ('button_parameter', models.CharField(help_text=b'A special parameter for the URL, leave blank if unsure.', max_length=40, null=True, blank=True)),
            ],
            options={
                'ordering': ['ordering'],
            },
            bases=(models.Model,),
        ),
    ]
