from django.conf.urls import patterns, include, url
from django.contrib import admin
from general import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'PatientCare.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

     url(r'^$', views.index),
     url(r'^general/', include('general.urls')),
     url(r'^admin/', include(admin.site.urls)),
     url(r'^accounts/login/$', views.index),

)
