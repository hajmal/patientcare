from django.shortcuts import render

# Create your views here.


from django.http import HttpResponse
from django.template import RequestContext, loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate , login,logout
from django.core.context_processors import csrf
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from models import doctor, patient ,visit, vaccine, vaccination, report, patientgrowth, medicine, Prevention, symptom, Dose
from systemform import doctorForm, patientForm, visitForm , vaccinationFormSchedule, vaccineForm, vaccinationupdate,reportForm, symptomForm,  vaccinationFormDone , PreventionForm , medicineForm , DoseForm
from django.http import QueryDict
from datetime import datetime
from datetime import timedelta
from django.utils import timezone
import pytz
from django.core import serializers
import json
import re
from smsthread import SMSThread
from datetime import date
from django.http import JsonResponse



def signupdoctor(request):
    if request.method == 'POST':
        
        try:
            #print request.POST
            form = doctorForm(request.POST)
            username = request.POST['username']
            doc = doctor.objects.all().filter(username=username)
                
            #print doc
            if len(doc) > 0:
                form = doctorForm()
                template = loader.get_template('general/signup.html')
                context = RequestContext(request, {'form':form, 'error': "User already exists. Please choose a different username!"})
                return HttpResponse(template.render(context))

            if form.is_valid():
                print form.save()
                username = request.POST['username']
                
                password = request.POST['password']
                email = request.POST['email']
                user = User.objects.create_user(username, email, password)
                    
                user = authenticate(username=username, password=password)
                login(request, user)
         
                return redirect('/general/home')
            else:
                        
                param = {'form':form}
                #param.update(csrf(request))
                return render(request,'general/signup.html',param)

        except:
            import traceback
            print traceback.format_exc()
            return HttpResponse("Error")

    form = doctorForm()
    template = loader.get_template('general/signup.html')
    context = RequestContext(request, {'form':form})
    return HttpResponse(template.render(context))


@login_required
def editdoc(request):
    
    instance = doctor.objects.get( doctor_id=request.session['doctor_id'])
    
    if request.method == "POST":
        form =  doctorForm(request.POST, instance=instance)

        if form.is_valid():
            print form.save()
            print "form is valid"

            return redirect('/general/home/')
        else:
            print form.errors
            print "form is invalid"

    else:
        form = doctorForm(instance=instance)

    param ={ 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form }

    return render(request,'general/editdoc.html',param)


@login_required
def showhome(request):

    user = request.user
    
    doc = doctor.objects.get(username=user.username)
    
    request.session['doctor_id'] = doc.doctor_id
    
    
    request.session['first_name'] = doc.first_name
    request.session['last_name'] = doc.last_name
    
    
    
    tzname = doc.timezone
        
    if tzname:
        timezone.activate(pytz.timezone(tzname))
    else:
        timezone.deactivate()


    try:
        error = request.GET['error']
    except:
        error = ""

    param = {'first_name': doc.first_name, 'last_name':doc.last_name, 'error':error }
    param.update(csrf(request))
            
    return render(request,'general/home.html',param)





def index(request):
    #   latest_question_list = Question.objects.order_by('-pub_date')[:5]
    #  output = ', '.join([p.question_text for p in latest_question_list])
    args = {}
    try:
        error = request.GET['err']
        args = {'Err': error}
    except:
        pass
    print "index"

    if request.user.is_authenticated():
        return redirect('/general/home')

    template = loader.get_template('general/login.html')
    context = RequestContext(request, args)
    return HttpResponse(template.render(context))



def process_login(request):
 
    if request.method == 'POST':
        
        try:
            username = request.POST['username']
            password = request.POST['password']
        except:
            return HttpResponse("Something went wrong")
        
        user = authenticate(username=username, password=password)
        if user is not None:
    
            if user.is_active:
                print "Active user found"
                param = {'first_name': user.first_name, 'last_name':user.last_name}
                param.update(csrf(request))
                
                doc = doctor.objects.get(username=username)
                request.session['doctor_id'] = doc.doctor_id
                
                request.session['first_name'] = doc.first_name
                request.session['last_name'] = doc.last_name
                
                login(request, user)
                return redirect('/general/home') #render(request,'general/home.html',param)
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            param = {'Err': 'User not autheticated'}
            
            print "ERRORED"
            #return render(request,'general',param)
            return redirect("/general/?err=User Authentication failed")


@login_required
def patients(request, id):

    mareez= None
    try:
        mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])
    
    except:
        import traceback
        print traceback.format_exc()
        return HttpResponse("SOME ERROR OCCURED")


    visits = visit.objects.all().filter(doctor_id=request.session['doctor_id'], patient_id=id).order_by('-date_visited')


    #print mareez ,len(id)
    param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'visits':visits}

    return render(request,'general/patient.html', param)


def makevisitdict(post):

    qdict = QueryDict('', mutable=True)
    qdict.update(post)


    for keys in qdict:

        print keys
    
        if qdict[keys] == "":
            if keys != 'prescription':
                if keys in ['fever', 'blood_pressure_lower', 'blood_pressure_higher', 'sugar' , 'height', 'weight' , 'hcf' , 'bmi' , 'fee']:
                    qdict[keys] = 0
    
                elif keys in ['date_visit_due']:
                    qdict[keys] = "1900-01-01"
    
                else:
                    qdict[keys] = " "

        print qdict[keys]

    return qdict

    """
    try:
        if qdict['symptoms'] == "":
            qdict['symptoms'] = "NOT FILLED"
    except:
        qdict['symptoms'] = "NOT FILLED"

    try:
        if qdict['presciption'] == "":
            qdict['prescription'] = "NOT FILLED"
    except:
        qdict['prescription'] = "NOT FILLED"

    try:

        if qdict ['blood_pressure_lower'] == "":
            qdict['blood_pressure_lower'] = "NOT FILLED"

    except:
        qdict['blood_pressure_lower'] = "NOT FILLED"


    try:
        if qdict ['blood_pressure_higher'] == ""
            qdict ['blood_pressure_higher'] = "NOT FILLED"
    except:
        qdict['blood_pressure_higher'] = "NOT FILLED"

    try:
        if qdict [''] == ""
            qdict ['blood_pressure_higher'] = "NOT FILLED"
    except:
        qdict['blood_pressure_higher'] = "NOT FILLED"

    """


@login_required
def deletepatient(request, pid):
    
    try:
        instance =  patient.objects.get(patient_id=pid, doctor_id=request.session['doctor_id'])
        instance.delete()
    except:
         pass
    
    return redirect('/general/showpatientslist')


@login_required
def newvisit(request, id):


    mareez= None
    try:
        mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    except:
        import traceback
        print traceback.format_exc()
        return HttpResponse("SOME ERROR OCCURED")

    if request.method == 'POST':
        
        #qdict = QueryDict('', mutable=True)
        #qdict.update(request.POST)
        
        qdict = makevisitdict(request.POST)
        
        form = visitForm(qdict)
        
        print "in newvisit post"
        if form.is_valid():
    
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.instance.patient_id = id

            gr = patientgrowth(doctor_id = request.session['doctor_id'], patient_id = id, height = form.instance.height, weight = form.instance.weight)
            gr.save()
            form.save()

            #### set sms reminder
            try:
                #datetime_object = datetime.strptime(form.instance.date_visit_due, '%Y-%m-%d')
                if (date.today() >= form.instance.date_visit_due):
                    # Now call sms API
                    todate = form.instance.date_visit_due.strftime('%d-%m-%Y')
                    sms = SMSThread(todate, mareez.phone, form.instance.next_visit_reminder, request.session['first_name'] + " " +  request.session['last_name'], mareez.first_name + " " +mareez.last_name)
                    sms.setName("SMSThread")
                    sms.start()

                    """ 
                    req = 'http://sendpk.com/api/sms.php?username=923334642113&password=9845&sender=PatientCare&date=' +todate+'&time=16:34:00&mobile='+mareez.phone +'&message=' + form.instance.next_visit_reminder
                    print req 
                    response = urllib2.urlopen(req)
                    print response.info()
                    html = response.read()
                    # do something
                    response.close()  # best practice to close the file"""

            except:
                import traceback
                print traceback.format_exc()
            ####
            return redirect('/general/patient/'+id+'/')

        else:
    
             param = {'form':form}
             #param.update(csrf(request))
             #return render(request,'general/newvisit.html',param)



    if request.method == 'GET':
        form = visitForm()



    #print form

    medicines = medicine.objects.all().filter(doctor_id=request.session['doctor_id'])
    preventions = Prevention.objects.all().filter(doctor_id=request.session['doctor_id'])
    symptoms = symptom.objects.all().filter(doctor_id=request.session['doctor_id'])
    doses = Dose.objects.all().filter(doctor_id=request.session['doctor_id'])
   
 
    param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form, 'medicines' : medicines, 'preventions':preventions, 'symptoms':symptoms, 'doses': doses }
    
    return render(request,'general/newvisit.html',param)



@login_required
def printvisit(request, id, vid):

    instance = visit.objects.get(id=vid, doctor_id=request.session['doctor_id'])

    

    doc = doctor.objects.get(doctor_id= request.session['doctor_id'])
    
    mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])


    param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'doctor':doc, 'visit': instance}

    return render(request,'general/printvisit.html',param)

#####
##### SMS Presecription
#####

@login_required
def smsvisit(request, id, vid):

    instance = visit.objects.get(id=vid, doctor_id=request.session['doctor_id'])

    doc = doctor.objects.get(doctor_id= request.session['doctor_id'])
    
    mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    #param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'doctor':doc, 'visit': instance}


    try:
            # Now call sms API
            todate = date.today().strftime('%d-%m-%Y')
            sms = SMSThread(None, mareez.phone, instance.prescription, request.session['first_name'] + " " +  request.session['last_name'],
                  mareez.first_name + " " + mareez.last_name)
            sms.setName("SMSThread")
            sms.start()

    except:
        import traceback
        print traceback.format_exc()

    return JsonResponse({'response':'ok'})



@login_required
def editvisit(request, id, vid):
    
    instance = visit.objects.get(id=vid, doctor_id=request.session['doctor_id'])
    mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    if request.method == "POST":
        qdict = makevisitdict(request.POST)
        form =  visitForm(qdict, instance=instance)

        if form.is_valid():
            form.save()

            return redirect('/general/patient/'+id+'/')

    else:
        form = visitForm(instance=instance)


    medicines = medicine.objects.all().filter(doctor_id=request.session['doctor_id']).order_by("name")
    preventions = Prevention.objects.all().filter(doctor_id=request.session['doctor_id'])
    symptoms = symptom.objects.all().filter(doctor_id=request.session['doctor_id'])
    doses = Dose.objects.all().filter(doctor_id=request.session['doctor_id'])

    param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form , 'visit': instance,  'medicines' : medicines, 'preventions':preventions, 'symptoms':symptoms, 'doses': doses}

    return render(request,'general/editvisit.html',param)



@login_required
def editpatient(request, id):
    instance = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    if request.method == "POST":
       form = patientForm(request.POST, instance=instance)
    
       if form.is_valid():
           form.save(commit=False)
           #    form.instance.doctor_id = request.session['doctor_id']
           form.save()

           return redirect('/general/patient/'+id+'/')

    else:
        form = patientForm(instance=instance)


    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form, 'last_patient' : instance}

    return render(request,'general/editpatient.html',param)


@login_required
def newpatient(request):
    alert = ""
    last_patient = None
    if request.method == 'POST':
        
        
        #print request.POST
        form = patientForm(request.POST)
        #username = request.POST['username']
        #doc = doctor.objects.all().filter(username=username)
            
            #print form
        last_patient = None
        if form.is_valid():
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.save()
            last_patient = form.instance
            alert = "Patient added successfully"
            print alert
        else:
                
            param = {'form':form}
                #param.update(csrf(request))
            return render(request,'general/AddPatient.html',param)


    doctor_id =  request.session['doctor_id']
    user= request.user
    doc = doctor.objects.get(username=user.username)

    form = patientForm(initial={'doctor_id': doctor_id})
    print form
    param = {'first_name': doc.first_name, 'last_name':doc.last_name, 'form':form, 'alert': alert, 'last_patient':last_patient}
    param.update(csrf(request))
    
    return render(request,'general/AddPatient.html',param)


@login_required
def showpatients(request):

    if request.method == 'GET':
        return redirect('/general/home/')

    if request.method == 'POST':
        print request.POST
        try:
            patientID= request.POST['patientID']
        except:
            patientID = ""
        
        try:

            cnic = request.POST['cnic']

        except:
            cnic = ""
        try:
            phone = request.POST['phone']
        except:

            phone = ""

        try:
            firstname = request.POST['firstname']
        except:
            firstname = ""

        if patientID == "" and  cnic == "" and phone == "" and firstname == "":
            csrf(request)
            return redirect('/general/home/?error=All fields are blank, fill atleast one')


        if patientID != "":
            patients = patient.objects.all().filter(patient_manual_id=patientID, doctor_id=request.session['doctor_id'])

        elif cnic != "":
            patients = patient.objects.all().filter(CNIC=cnic  , doctor_id=request.session['doctor_id'])

        elif phone != "":
            patients = patient.objects.all().filter(phone=phone, doctor_id=request.session['doctor_id'])
  
        elif firstname != "":
            patients = patient.objects.all().filter(first_name=firstname, doctor_id=request.session['doctor_id'])

        else:
            patients = []



        param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'patients':patients}
        param.update(csrf(request))
    
        return render(request,'general/showpatient.html',param)

@login_required
def editvaccine(request, vacid):
    
    instance =  vaccine.objects.get(id=vacid, doctor_id=request.session['doctor_id'])
    
    if request.method == "POST":
        #qdict = makevisitdict(request.POST)
        form = vaccineForm(request.POST, instance=instance)

        if form.is_valid():
            form.save()

            return redirect('/general/showvaccine/')

    else:
        form = vaccineForm(instance=instance)

    param ={'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form , 'vac': instance}

    return render(request,'general/editvaccine.html',param)


@login_required
def removevaccine(request, vacid):
    
    instance =  vaccine.objects.get(id=vacid, doctor_id=request.session['doctor_id'])
    instance.delete()

    
    return redirect('/general/showvaccine')


@login_required
def showvaccine(request):
    
    vac = vaccine.objects.all().filter(doctor_id=request.session['doctor_id'])
    #instance = visit.objects.get(id=vid, doctor_id=request.session['doctor_id'])
    #mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    
    param ={ 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'vac':vac}

    return render(request,'general/showvaccine.html',param)


@login_required
def addvaccine(request):
    alert = ""
    if request.method == 'POST':
    
        form = vaccineForm(request.POST)
    
        if form.is_valid():
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.save()
            alert = "Vaccine added successfully"
        else:
            param = {'form':form}
    
            return render(request,'general/addvaccine.html',param)


    form = vaccineForm()
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form ,'alert': alert}
    param.update(csrf(request))
        
    return render(request,'general/addvaccine.html',param)


def makeapplyvaccinedict(post):

    qdict = QueryDict('', mutable=True)
    qdict.update(post)

    #pattern = re.compile("^([0-9]+)+$")
    pattern = re.compile("^(\d*\.)?\d+$")
    for keys in qdict:

        print keys
        if keys in ['height', 'weight']:
           
            if len (qdict[keys]) == 0:
                qdict[keys] = 0

    
            if pattern.match(qdict[keys]) == None:
                qdict[keys] = 0
            
            elif qdict[keys] != 0:
                pass
            else :
                qdict[keys] = 0

            
        if keys in ['remarks']:
            if qdict[keys] == "":
                qdict[keys] = "NOT-FILLED"


    return qdict


@login_required
def applyvaccine(request, id):
    
    mareez= None
    errors = ""
    try:
        mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    except:
        import traceback
        print traceback.format_exc()
        return HttpResponse("SOME ERROR OCCURED")
    
    
    if request.method == 'POST':


        qdict = makeapplyvaccinedict(request.POST)

        print request.POST
        form = vaccinationFormDone(qdict)
    



        if form.is_valid():
            
            
          #  vaccinetype = [(x.vaccine_type) for x in vaccine.objects.all().filter(doctor_id=request.session['doctor_id'])][int(form['VaccineType'].value())]
            
           # vaccinemedicine =  [(x.vaccine_medicine) for x in vaccine.objects.all().filter(doctor_id=request.session['doctor_id'])][int(form['VaccineMedicine'].value())]
            
            vaccinetype = form['VaccineType'].value()
            #vaccinemedicine = form['VaccineMedicine'].value()
            vid= form['VaccineID'].value()
            date = form['date_vaccine_due'].value()
            phone = form['phone'].value()
            age = form['age'].value()
            vid= form['VaccineID'].value()
            date_vaccinated = form['date_vaccinated'].value()
            height = form['height'].value()
            weight = form['weight'].value()
            remarks = form['remarks'].value()
            is_vaccinated = form['is_vaccinated'].value()
            date_obj = datetime.strptime(date,'%Y-%m-%d').date()
            date_obj_vnated = datetime.strptime(date,'%Y-%m-%d').date()
            
            #v = vaccination(doctor_id = request.session['doctor_id'], patient_id = id, vaccine_type = vaccinetype, vaccine_medicine = vaccinemedicine, phone=phone, date_vaccine_due=date_obj, vaccine_id=vid)
            vlist = vaccination.objects.all().filter(patient_id=id, doctor_id=request.session['doctor_id'], id = vid)
            v = vlist[0]
            v.phone = phone
            v.date_vaccine_due = date
            v.height = height
            v.weight = weight
            v.remarks = remarks
            if date_obj_vnated is not None:
                v.date_vaccinated = date_obj_vnated
            if is_vaccinated == "on":
                v.is_vaccinated = 1 
            #print v.height
            pr = patientgrowth(doctor_id = request.session['doctor_id'], patient_id = id, height = v.height, weight = v.weight)
            pr.save()

            v.save()
    
            #return redirect('/general/patient/'+id+'/')
        else:
            print "form is not valid"
            print form.errors
            errors = form.errors
    #if request.method == 'GET':
        #form = vaccinationFormSchedule(request.session['doctor_id'])
    vac = vaccine.objects.all().filter(doctor_id=request.session['doctor_id'])

    pvac = vaccination.objects.all().filter(patient_id=id, doctor_id=request.session['doctor_id'])

    pvac_list  = (o.vaccine_id for o in pvac)

    if pvac_list != None:
         for v in vac:
            #print v.id
            if v.id not in pvac_list:
                #print str(v.id) + " no"
                v = vaccination(doctor_id = request.session['doctor_id'], patient_id = id, vaccine_type = v.vaccine_type, vaccine_medicine = v.vaccine_medicine,  vaccine_id=v.id , age=v.age, vaccine_reminder=v.vaccine_reminder)
                v.save()
    else:
        for v in vac:
            v = vaccination(doctor_id = request.session['doctor_id'], patient_id = id, vaccine_type = v.vaccine_type, vaccine_medicine = v.vaccine_medicine,  vaccine_id=v.id, age=v.age)
            v.save() 
        

    upv = vaccination.objects.all().filter(patient_id=id, doctor_id=request.session['doctor_id'])
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'patient':mareez, 'vaccs': upv, 'errors' : errors}
    
    return render(request,'general/applyvaccine.html',param)

@login_required
def schedulevaccine(request, id):

    mareez= None
    errors = []
    try:
        mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    except:
        import traceback
        print traceback.format_exc()
        return HttpResponse("SOME ERROR OCCURED")
    
    
    if request.method == 'POST':


        print request.POST
        form = vaccinationFormSchedule(request.POST)
    
        if form.is_valid():
            vaccinetype = form['VaccineType'].value()
            #vaccinemedicine = form['VaccineMedicine'].value()
            vid= form['VaccineID'].value()
            datef = form['date_vaccine_due'].value()
            phone = form['phone'].value()
            age = form['age'].value()
            vid= form['VaccineID'].value()
            date_obj = datetime.strptime(datef,'%Y-%m-%d').date()
           
            vlist = vaccination.objects.all().filter(patient_id=id, doctor_id=request.session['doctor_id'], id = vid)
            v = vlist[0]
            v.phone = phone
            v.date_vaccine_due = datef
                       
            v.save()

          

            try:
                vacm = vaccine.objects.get(id=vid, doctor_id=request.session['doctor_id'])
                if (date.today() >= date_obj):
                    # Now call sms API
                    todate = date_obj.strftime('%d-%m-%Y')
                    sms = SMSThread(todate, mareez.phone, vacm.vaccine_reminder, request.session['first_name'] + " " +  request.session['last_name'], mareez.first_name + " "+  mareez.last_name)
                    sms.setName("SMSThread")
                    sms.start()

          
            except:
                import traceback
                print traceback.format_exc()
    
            #return redirect('/general/patient/'+id+'/')
        else:
            print "form is not valid"
            print form.errors
            errors = form.errors
    #if request.method == 'GET':
        #form = vaccinationFormSchedule(request.session['doctor_id'])
    vac = vaccine.objects.all().filter(doctor_id=request.session['doctor_id'])

    pvac = vaccination.objects.all().filter(patient_id=id, doctor_id=request.session['doctor_id'])

    pvac_list  = (o.vaccine_id for o in pvac)

    if pvac_list != None:
         for v in vac:
            #print v.id
            if v.id not in pvac_list:
                #print str(v.id) + " no"
                v = vaccination(doctor_id = request.session['doctor_id'], patient_id = id, vaccine_type = v.vaccine_type, vaccine_medicine = v.vaccine_medicine,  vaccine_id=v.id , age=v.age)
                v.save()
    else:
        for v in vac:
            v = vaccination(doctor_id = request.session['doctor_id'], patient_id = id, vaccine_type = v.vaccine_type, vaccine_medicine = v.vaccine_medicine,  vaccine_id=v.id, age=v.age)
            v.save() 
        

    upv = vaccination.objects.all().filter(patient_id=id, doctor_id=request.session['doctor_id'])
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'patient':mareez, 'vaccs': upv , 'errors' : errors}
    
    return render(request,'general/applyvaccine.html',param)



@login_required
def updatevaccination(request, id):

    vac = vaccination.objects.all().filter(doctor_id = request.session['doctor_id'], patient_id = id)

    formdict = {}
    
    i = 0

    #for v in vac:

    #    formdict['form'+str(i)] = vaccinationupdate(v)


    try:
        mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])
    
    except:
        import traceback
        print traceback.format_exc()
        return HttpResponse("SOME ERROR OCCURED")


    today = datetime.now().date()
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'patient':mareez, 'vaccs': vac, 'today' : today}

    return render(request,'general/updatevaccination.html',param)



@login_required
def updatevaccinationdata(request, id, vid):
    
    if request.method == 'POST':
        try:
            is_vaccinated  = request.POST['is_vaccinated']
        except:
            is_vaccinated  = ""
        
        try:
            datev = request.POST['date_vaccinated']
        except:
            datev = ""

        vac = vaccination.objects.get(id=vid, patient_id = id, doctor_id= request.session['doctor_id'])

        if is_vaccinated == "on":
            vac.is_vaccinated = 1

        if datev != "":
            vac.date_vaccinated = datetime.strptime(datev,'%Y-%m-%d').date()


        vac.save()


        param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] }
        param.update(csrf(request))
    

        return redirect("/general/updatevaccination/"+id+"/")


    return HttpResponse("you are here due to some error")


def makereportdict(post):

    qdict = QueryDict('', mutable=True)
    qdict.update(post)


    for keys in qdict:

        print keys
    
        if qdict[keys] == "":
            qdict[keys] = "NOTFILLED"

        print qdict[keys]

    return qdict


@login_required
def addreport(request, id):

    mareez= None
    try:
        mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    except:
        import traceback
        print traceback.format_exc()
        return HttpResponse("SOME ERROR OCCURED")

    if request.method == 'POST':
        
        #qdict = QueryDict('', mutable=True)
        #qdict.update(request.POST)
        
        qdict = makereportdict(request.POST)
        
        form = reportForm(qdict)
        
        print "in addreport post"
        if form.is_valid():
    
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.instance.patient_id = id
            print form.save()
            return redirect('/general/showreport/'+id+'/')

        else:
    
             param = {'form':form}
             #param.update(csrf(request))
             #return render(request,'general/newvisit.html',param)


    if request.method == 'GET':
        form = reportForm()

    param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form}
    
    return render(request,'general/addreport.html', param)



@login_required
def showreport(request, id):
    mareez= None
    try:
        mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])
    
    except:
        import traceback
        print traceback.format_exc()
        return HttpResponse("SOME ERROR OCCURED")


    reports = report.objects.all().filter(doctor_id=request.session['doctor_id'], patient_id=id)


    #print mareez ,len(id)
    param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'reports':reports}

    return render(request,'general/showreport.html', param)

@login_required
def editreport(request, id, rid):

    instance = report.objects.get(id=rid, doctor_id=request.session['doctor_id'])
    mareez = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    if request.method == "POST":
        qdict = makereportdict(request.POST)
        form =  reportForm(qdict, instance=instance)

        if form.is_valid():
            form.save()

            return redirect('/general/showreport/'+id+'/')

    else:
        form = reportForm(instance=instance)

    param ={'patient':mareez, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form , 'report': instance}

    return render(request,'general/editreport.html',param)

@login_required
def showcharts(request, id):
    instance = patient.objects.get(patient_id=id, doctor_id=request.session['doctor_id'])

    growth = patientgrowth.objects.all().filter(doctor_id=request.session['doctor_id'], patient_id=id)
    visits = visit.objects.all().filter(doctor_id=request.session['doctor_id'], patient_id=id)
    

    param = {'patient':instance, 'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'visits':visits, 'growthlist' : growth}

    return render(request,'general/showcharts.html',param)


#@login_required
def getallvaccinesdue(request):
        
    try:
        print request.GET
        username = request.GET['username']
        password = request.GET['password']
    except:
        return HttpResponse("Something went wrong")
        
    user = authenticate(username=username, password=password)
    if user is not None:
    
        if user.is_active:
            print "Active user found"
            param = {'first_name': user.first_name, 'last_name':user.last_name}
            param.update(csrf(request))
                
            doc = doctor.objects.get(username=username)
            request.session['doctor_id'] = doc.doctor_id
                
            request.session['first_name'] = doc.first_name
            request.session['last_name'] = doc.last_name
                
            login(request, user)

            today = datetime.now()
            tomorrow = today + timedelta(1,0)
            vaccine = vaccination.objects.all().filter(doctor_id=request.session['doctor_id']  , date_vaccine_due=tomorrow ,is_vaccinated=0).defer("vaccine_reminder")
    
            data = {'first_name': doc.first_name, 'last_name':doc.last_name}

            for v in vaccine:
                data[v.phone]=v.vaccine_reminder

            data = json.dumps(data)

            print data

            return HttpResponse(data, content_type='application/json')


#@login_required
def getallvisitssdue(request):

    try:
        username = request.GET['username']
        password = request.GET['password']
    except:
        return HttpResponse("Something went wrong")
        
    user = authenticate(username=username, password=password)
    if user is not None:
    
        if user.is_active:
            print "Active user found"
            param = {'first_name': user.first_name, 'last_name':user.last_name}
            param.update(csrf(request))
                
            doc = doctor.objects.get(username=username)
            request.session['doctor_id'] = doc.doctor_id
                
            request.session['first_name'] = doc.first_name
            request.session['last_name'] = doc.last_name
                
            login(request, user)

            today = datetime.now()
            tomorrow = today + timedelta(1,0)
            visits = visit.objects.all().filter(doctor_id=request.session['doctor_id'], date_visit_due=tomorrow)    

            data = {'first_name': doc.first_name, 'last_name':doc.last_name}
            for v in visits:
                mareez = patient.objects.get(patient_id=v.patient_id, doctor_id=request.session['doctor_id'])
                data[mareez.phone]=v.next_visit_reminder

            data = json.dumps(data)

            print data

            return HttpResponse(data, content_type='application/json')



@login_required
def addsymptom(request):
    alert = ""
    if request.method == 'POST':
    
        form = symptomForm(request.POST)
    
        if form.is_valid():
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.save()
            alert = "Symptom added successfully"
        else:
            param = {'form':form}
    
            return render(request,'general/addsymptom.html', param)


    form = symptomForm()
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form ,'alert': alert}
    param.update(csrf(request))
        
    return render(request,'general/addsymptom.html',param)



@login_required
def showsymptoms(request):
    symptoms = symptom.objects.all().filter(doctor_id=request.session['doctor_id'])
    
    param ={ 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'symptoms':symptoms}

    return render(request,'general/showsymptoms.html',param)
   
@login_required
def addprevention(request):
    alert = ""
    if request.method == 'POST':
    
        form = PreventionForm(request.POST)
    
        if form.is_valid():
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.save()
            alert = "Prevention added successfully"
        else:
            param = {'form':form}
    
            return render(request,'general/addprevention.html', param)


    form = PreventionForm()
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form ,'alert': alert}
    param.update(csrf(request))
        
    return render(request,'general/addprevention.html',param)



@login_required
def showpreventions(request):

    preventions = Prevention.objects.all().filter(doctor_id=request.session['doctor_id'])
    
    param ={ 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'preventions':preventions}

    return render(request,'general/showpreventions.html',param)


@login_required
def addmedicine(request):
    alert = ""
    if request.method == 'POST':
    
        form = medicineForm(request.POST)
    
        if form.is_valid():
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.save()
            alert = "Medicine added successfully"
        else:
            param = {'form':form}
    
            return render(request,'general/addmedicine.html', param)


    form = medicineForm()
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form ,'alert': alert}
    param.update(csrf(request))
        
    return render(request,'general/addmedicine.html',param)


@login_required
def showmedicines(request):

    medicines = medicine.objects.all().filter(doctor_id=request.session['doctor_id']).order_by("name")
    
    param ={ 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'medicines':medicines}

    return render(request,'general/showmedicines.html',param)

@login_required
def editmedicine(request, mid):
    
    instance =  medicine.objects.get(id=mid, doctor_id=request.session['doctor_id'])
   
    if request.method == "POST":
        #qdict = makevisitdict(request.POST)
        form = medicineForm(request.POST, instance=instance)

        if form.is_valid():
            form.save()

            return redirect('/general/showmedicines/')

    else:
        form = medicineForm(instance=instance)

    param ={'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form , 'med': instance}

    return render(request,'general/editmedicine.html',param)

@login_required
def deletemedicine(request, mid):
    
    instance =  medicine.objects.get(id=mid, doctor_id=request.session['doctor_id'])
    instance.delete()

    
    return redirect('/general/showmedicines')


@login_required
def editprevention(request, pid):
    
    instance =  Prevention.objects.get(id=pid, doctor_id=request.session['doctor_id'])
   
    if request.method == "POST":
        #qdict = makevisitdict(request.POST)
        form = PreventionForm(request.POST, instance=instance)

        if form.is_valid():
            form.save()

            return redirect('/general/showpreventions/')

    else:
        form = medicineForm(instance=instance)

    param ={'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form , 'pre': instance}

    return render(request,'general/editprevention.html',param)

@login_required
def deleteprevention(request, pid):
    
    instance =  Prevention.objects.get(id=pid, doctor_id=request.session['doctor_id'])
    instance.delete()

    
    return redirect('/general/showpreventions')



@login_required
def editsymptom(request, sid):
    
    instance =  symptom.objects.get(id=sid, doctor_id=request.session['doctor_id'])
   
    if request.method == "POST":
        #qdict = makevisitdict(request.POST)
        form = symptomForm(request.POST, instance=instance)

        if form.is_valid():
            form.save()

            return redirect('/general/showsymptoms/')

    else:
        form = symptomForm(instance=instance)

    param ={'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form , 'sym': instance}

    return render(request,'general/editsymptom.html',param)

@login_required
def deletesymptom(request, sid):
    
    instance =  symptom.objects.get(id=sid, doctor_id=request.session['doctor_id'])
    instance.delete()
  
    return redirect('/general/showsymptoms')


@login_required
def showpatientslist(request):

    patients = patient.objects.all().filter(doctor_id=request.session['doctor_id'])
    
    param ={ 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'patients':patients}

    return render(request,'general/showpatients.html',param)


@login_required
def adddose(request):
    alert = ""
    if request.method == 'POST':
    
        form = DoseForm(request.POST)
    
        if form.is_valid():
            form.save(commit=False)
            form.instance.doctor_id = request.session['doctor_id']
            form.save()
            alert = "Dose added successfully"
        else:
            param = {'form':form}
    
            return render(request,'general/adddose.html', param)


    form = DoseForm()
    param = {'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form ,'alert': alert}
    param.update(csrf(request))
        
    return render(request,'general/adddose.html',param)

@login_required
def showdoses(request):

    doses = Dose.objects.all().filter(doctor_id=request.session['doctor_id'])
    
    param ={ 'first_name':request.session['first_name'], 'last_name':request.session['last_name'], 'doses':doses}

    return render(request,'general/showdoses.html',param)

@login_required
def editdose(request, mid):
    
    instance =  Dose.objects.get(id=mid, doctor_id=request.session['doctor_id'])
   
    if request.method == "POST":
        #qdict = makevisitdict(request.POST)
        form = DoseForm(request.POST, instance=instance)

        if form.is_valid():
            form.save()

            return redirect('/general/showdoses/')

    else:
        form = DoseForm(instance=instance)

    param ={'first_name':request.session['first_name'], 'last_name':request.session['last_name'] , 'form':form , 'dose': instance}

    return render(request,'general/editdose.html',param)


@login_required
def deletedose(request, mid):
    
    instance =  Dose.objects.get(id=mid, doctor_id=request.session['doctor_id'])
    instance.delete()

    
    return redirect('/general/showdoses')

@login_required
def logdoc_out(request):
    logout(request)
    return redirect('/general/')

