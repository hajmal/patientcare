from django import forms

class newPatientForm(forms.Form):
    firstname = forms.CharField()
    email = forms.EmailField()
    lastname = forms.CharField()
    dob = forms.DateField
