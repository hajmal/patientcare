from django import forms
from django.forms import ModelForm
from  models import doctor , patient, visit, vaccine, vaccination, report, medicine, Prevention, symptom , Dose
import pytz

class doctorForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs = {'placeholder': 'Password'}))
 
 
    class Meta:
        model = doctor
        fields = ['username',  'first_name', 'last_name', 'address', 'city', 'phone','acheivements','timezone', 'specialization', 'email']#, 'qualification']

        widgets = {
            
                 'username' : forms.TextInput(attrs = {'placeholder': 'Username'}),                 
                 'first_name' : forms.TextInput(attrs = {'placeholder': 'First name'}),
                 'last_name' : forms.TextInput(attrs = {'placeholder': 'Last name'}),
                 'acheivements' : forms.TextInput(attrs = {'placeholder': 'Distinction'}),
                 'address' : forms.TextInput(attrs = {'placeholder': 'address'}),
                 'city' : forms.TextInput(attrs = {'placeholder': 'City'}),
                 'phone' : forms.TextInput(attrs = {'placeholder': 'Phone'}),
                 'email'    : forms.TextInput(attrs = {'placeholder': 'Email'}),
                 #'qualification'  : forms.TextInput(attrs = {'placeholder': 'Qualification - This will appear on prescription'}),
                 
                
                 }





        #self.fields['description'].widget= forms.TextInput(attrs={'placeholder': u'Bla bla'})


class patientForm(ModelForm):
    class Meta:
        model = patient
        fields = ['patient_manual_id', 'first_name', 'last_name', 'date_of_birth', 'gender', 'address', 'phone', 'city', 'CNIC', 'allergic', 'special_notes']


        widgets = {
    
                 'patient_manual_id' : forms.TextInput(attrs = {'placeholder': 'ID'}),
                 'date_of_birth' : forms.TextInput(attrs = {'placeholder': 'Date of Birth'}),
                  'first_name' : forms.TextInput(attrs = {'placeholder': 'First name'}),
                  'last_name' : forms.TextInput(attrs = {'placeholder': 'Last name'}),
                  'address' : forms.TextInput(attrs = {'placeholder': 'address'}),
                  'city' : forms.TextInput(attrs = {'placeholder': 'City'}),
                  'phone' : forms.TextInput(attrs = {'placeholder': 'Mobile (format : 923331234567)'}),
                  #'gender'    : forms.TextInput(attrs = {'placeholder': 'Gender'}),
                  'CNIC':  forms.TextInput(attrs = {'placeholder': 'ID CARD'}),
                  'allergic':  forms.TextInput(attrs = {'placeholder': 'Allergies'}),
                  'special_notes':  forms.TextInput(attrs = {'placeholder': 'NOTES'}),
                                
                            }


class visitForm(ModelForm):
    class Meta:
        model = visit
        fields = [ 'symptoms',  'blood_pressure_lower', 'blood_pressure_higher', 'fever', 'sugar', 'height', 'weight','hcf','bmi','prescription', 'note','date_visit_due' , 'next_visit_reminder', 'fee']
        
        
        widgets = {
            
            
                  'symptoms' : forms.Textarea(attrs = {'placeholder': 'Add Symptoms from above dropdown'}),                  
                  'blood_pressure_lower' : forms.TextInput(attrs = {'placeholder': 'Blood Pressure Lower'}),
                  'blood_pressure_higher' : forms.TextInput(attrs = {'placeholder': 'Blood Pressure Higher'}),
                  'fever' : forms.TextInput(attrs = {'placeholder': 'Fever'}),
                  'sugar'    : forms.TextInput(attrs = {'placeholder': 'Sugar'}),
                  'height' : forms.TextInput(attrs = {'placeholder': 'Height'}),
                  'weight'    : forms.TextInput(attrs = {'placeholder': 'Weight'}),
                  'hcf' : forms.TextInput(attrs = {'placeholder': 'Head Cicumference'}),
                  'bmi'    : forms.TextInput(attrs = {'placeholder': 'Body Mass Index'}),
                  'prescription' : forms.Textarea(attrs = {'placeholder': 'Prescription'}),
                  'date_visit_due':  forms.TextInput(attrs = {'placeholder': 'Date of next visit'}),
                  'note' : forms.Textarea(attrs = {'placeholder': 'Important notes'}),
                  'next_visit_reminder' : forms.Textarea(attrs = {'placeholder': 'Next visit reminder message'}),
                  'fee': forms.TextInput(attrs = {'placeholder': 'Fee'}),
                                            
                                        }

class vaccineForm(ModelForm):
    class Meta:
        model= vaccine
        fields = ['vaccine_type' , 'vaccine_medicine', 'age', 'vaccine_reminder']

        widgets = {
                    'vaccine_type' : forms.TextInput(attrs = {'placeholder': 'Vaccine Type'}),
                    'vaccine_medicine'    : forms.TextInput(attrs = {'placeholder': 'Medicine'}),
                    'age'    : forms.TextInput(attrs = {'placeholder': 'Age group'}),
                    'vaccine_reminder' : forms.Textarea(attrs = {'placeholder': 'Reminder message for this vaccine'}),

                  }

class vaccinationFormSchedule(forms.Form):
    age =  forms.CharField(widget=forms.TextInput())     
    VaccineType = forms.CharField(widget=forms.TextInput())
    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Phone'}))
    date_vaccine_due = forms.DateField(widget=forms.TextInput())
    VaccineID = forms.CharField(widget=forms.TextInput())

    #fields = ['phone', 'date_vaccine_due']



    #def __init__(self, doctor_id, *args, **kwargs):
             
   #     super(vaccinationFormSchedule, self).__init__(*args, **kwargs)
   #     self.fields['VaccineType'].choices = tuple(enumerate([(x.vaccine_type) for x in vaccine.objects.all().filter(doctor_id=doctor_id)]))
   #     self.fields['VaccineMedicine'].choices = tuple(enumerate([(x.vaccine_medicine) for x in vaccine.objects.all().filter(doctor_id=doctor_id)]))

class vaccinationFormDone(forms.Form):
   
         #model = vaccination
    age =  forms.CharField(widget=forms.TextInput())     
    VaccineType = forms.CharField(widget=forms.TextInput())
    # VaccineMedicine = forms.CharField(widget=forms.TextInput())
    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Phone'}))
    date_vaccine_due = forms.DateField(widget=forms.TextInput())
    height = forms.CharField(widget=forms.TextInput())
    weight = forms.CharField(widget=forms.TextInput())
    remarks = forms.CharField(widget=forms.TextInput())
    is_vaccinated = forms.CharField(widget=forms.TextInput())
    date_vaccinated = forms.DateField(widget=forms.TextInput())
    VaccineID = forms.CharField(widget=forms.TextInput())



class vaccinationupdate(forms.Form):

    vaccine_type = forms.CharField()
    vaccine_medicine = forms.CharField()
    phone = forms.CharField()
    date = forms.DateField()
    cost = forms.CharField()
    date_vaccinated = forms.DateField()

class reportForm(ModelForm):
    class Meta:
        model= report
        fields = [ 'report', 'findings']

        widgets = {
                    'report'    : forms.Textarea(attrs = {'placeholder': 'Report'}),
                    'findings'    : forms.Textarea(attrs = {'placeholder': 'Findings'}),
                    
                  }

class symptomForm(ModelForm):
    class Meta:
        model= symptom
        #fields = ['category' , 'name']
        fields = [ 'name']
        widgets = {
                    #'category' : forms.TextInput(attrs = {'placeholder': 'Symptom Category'}),
                    'name'    : forms.TextInput(attrs = {'placeholder': 'Name'}),
                    
                  }
class PreventionForm(ModelForm):
    class Meta:
        model= Prevention
        #fields = ['category' , 'name']
        fields = [ 'name']

        widgets = {
         
                    'name'    : forms.TextInput(attrs = {'placeholder': 'Name'}),
                    
                  }
class medicineForm(ModelForm):
    class Meta:
        model= medicine
        #fields = ['category' , 'name'] 
        fields = [ 'name']

        widgets = {
         
                    'name'    : forms.TextInput(attrs = {'placeholder': 'Name'}),
                    
                  }

class DoseForm(ModelForm):
    class Meta:
        model= Dose
        #fields = ['category' , 'name'] 
        fields = [ 'name']

        widgets = {
         
                    'name'    : forms.TextInput(attrs = {'placeholder': 'Name'}),
                    
                  }

