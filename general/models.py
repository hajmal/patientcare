from django.db import models
from datetime import datetime
import pytz
from django.utils.timezone import get_current_timezone, make_aware, utc
# Create your models here.

#GENDER_CHOICES = (('a','male'),('b','female'))
TZ_CHOICES = tuple([(tz,tz) for tz in pytz.all_timezones])

speciality = (('Paediatrician','Paediatrician'), ('Cardiologist','Cardiologist'))
gender_list = (('Male','Male'), ('Female','Female'))

def localize_datetime(dtime):
    """Makes DateTimeField value UTC-aware and returns datetime string localized
        in user's timezone in ISO format.
        """
    tz_aware = make_aware(dtime, utc).astimezone(get_current_timezone())
    return datetime.datetime.strftime(tz_aware, '%Y-%m-%d %H:%M:%S')


class doctor(models.Model):
    doctor_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    acheivements = models.CharField(max_length=1000, default = "")
    phone = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    email = models.EmailField()
    timezone = models.CharField(choices=TZ_CHOICES,max_length=500, default = 'Asia/Karachi')
    specialization = models.CharField(choices=speciality, max_length=500, default = 'Paediatrician')
    date_created = models.DateTimeField(auto_now_add=True)


class patient (models.Model):
    patient_id = models.AutoField(primary_key=True)
    doctor_id = models.IntegerField()
    patient_manual_id = models.CharField(max_length=100, default = "0")
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    date_of_birth = models.DateField()
    gender = models.CharField(choices=gender_list, max_length=200, default = 'Female') #models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    CNIC = models.CharField(max_length=200)
    allergic = models.CharField(max_length=6200, default="")
    special_notes = models.CharField(max_length=6200, default="")

class visit(models.Model):
    doctor_id = models.IntegerField()
    patient_id = models.IntegerField()
    note = models.CharField(max_length=5000,default="")
    symptoms = models.CharField(max_length=5000,default="")
    prescription = models.CharField(max_length=5000, default="")
    blood_pressure_lower = models.IntegerField(default=0)
    blood_pressure_higher = models.IntegerField(default=0)
    fever = models.IntegerField(default=0)
    sugar = models.IntegerField(default=0)
    hcf = models.IntegerField(default=0) #Head Cicumference
    bmi = models.IntegerField(default=0)
    height = models.FloatField(default=0)
    weight = models.FloatField(default=0)
    date_visited = models.DateField(auto_now_add=True)
    date_visit_due = models.DateField( )
    next_visit_reminder = models.CharField(max_length=5000,default="")
    fee = models.IntegerField()


class vaccination(models.Model):
    doctor_id = models.IntegerField()
    patient_id = models.IntegerField()
    vaccine_id = models.IntegerField(default=-1)
    vaccine_type = models.CharField(max_length=500)
    vaccine_medicine = models.CharField(max_length=500)
    vaccine_reminder = models.CharField(max_length=5000,default="")
    phone = models.CharField(max_length=1200)
    date_vaccinated = models.DateField(null=True)
    is_vaccinated = models.IntegerField(default=0)
    not_required = models.IntegerField(default=0)
    date_vaccine_due = models.DateField(null=True)
    cost = models.IntegerField(default=0)
    height = models.FloatField(default=0)
    weight = models.FloatField(default=0)
    remarks = models.CharField(max_length=500, default="")
    age = models.CharField(max_length=500, default="0")

class vaccine(models.Model):
    doctor_id = models.IntegerField()
    vaccine_type = models.CharField(max_length=500)
    vaccine_medicine = models.CharField(max_length=500)
    age = models.CharField(max_length=100)
    vaccine_reminder = models.CharField(max_length=5000,default="")

class patientSubmission(models.Model):
    doctor_id = models.IntegerField()
    patient_id = models.IntegerField()
    CNIC = models.CharField(max_length=200)
    blood_pressure_lower = models.IntegerField()
    blood_pressure_higher = models.IntegerField()
    fever = models.IntegerField()
    sugar = models.IntegerField()

class TestReport(models.Model):
    doctor_id = models.IntegerField()
    patient_id = models.IntegerField()
    CNIC = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    report_image = models.ImageField(max_length=2000)


class report(models.Model):
    doctor_id = models.IntegerField()
    patient_id = models.IntegerField()
    report = models.CharField(max_length=5000,default="")
    findings = models.CharField(max_length=5000,default="")
    date_report_added = models.DateField(auto_now_add=True)


class patientgrowth (models.Model):
    patient_id = models.IntegerField()
    doctor_id = models.IntegerField()
    dated = models.DateField(auto_now_add=True)
    height = models.FloatField(default=0)
    weight = models.FloatField(default=0)

class medicine (models.Model):
    doctor_id = models.IntegerField()
    #category = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name
    def __str__(self):
        return self.name.encode('utf8')

class Prevention(models.Model):
    doctor_id = models.IntegerField()
    #category = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

class symptom(models.Model):
    doctor_id = models.IntegerField()
    #category = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

class Dose(models.Model):
    doctor_id = models.IntegerField()
    #category = models.CharField(max_length=200)
    name = models.CharField(max_length=200)

    #  @staticmethod
    #  def getAllNamesInCategory(categ, doc_id):
    #     return symptom.objects.filter(doctor_id=doc_id, category=categ).values_list('name', flat=True)
        

